//First University NodeJS  Application

const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
    res.send('Hello Virtual Machine!')
})

app.get('/servicea', (req, res) => {
    res.send('Hello from a different endpoint!')
})

app.listen(port, () => {
    console.log(`Express Application Listening at port 3000`)
})